var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    rigger = require('gulp-rigger'),
    imagemin = require('gulp-imagemin');



gulp.task('default', ['css-min', 'js-min', 'image-min', 'copy-font', "copy-html"]);

gulp.task('css-min', function () {
    gulp.src("css/*.css")
        .pipe(concat('main.min.css'))
        .pipe(cssmin())
        .pipe(gulp.dest('../dist/css/'));
});

gulp.task('js-min',function () {
    gulp.src('js/main.js')
        .pipe(rigger())
        .pipe(uglify().on('error', function(e){
            console.log(e);
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('../dist/js/'))
});

gulp.task('image-min', function() {
    gulp.src('images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('../dist/images/'))
})

gulp.task('copy-font', function() {
    gulp.src('font/*')
        .pipe(gulp.dest('../dist/font/'))
})

gulp.task('copy-html', function() {
    gulp.src('*.html')
        .pipe(gulp.dest('../dist/'))
})


