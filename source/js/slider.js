function initSlider() {
    $('.preview-slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        dots: true,
        dotsClass: 'slider-pagination',
    });
};